#
# "$Id$"
#
#   RPM "spec" file for fldiff.
#
#   Copyright 2005 by Michael Sweet
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License v2 as published
#   by the Free Software Foundation.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#

Summary: fldiff
Name: fldiff
Version: 1.1
Release: 1
Copyright: GPL
Group: Development/Tools
Source: http://www.easysw.com/~mike/fldiff/fldiff-%{version}-source.tar.gz
Url: http://www.easysw.com/~mike/fldiff
Packager: Michael Sweet <mike@easysw.com>
Vendor: Michael Sweet

# Use buildroot so as not to disturb the version already installed
BuildRoot: /var/tmp/%{name}-root

%description

%prep
%setup
./configure --prefix=/usr --mandir=/usr/share/man

%build
make

%install
# Make sure the RPM_BUILD_ROOT directory exists.
rm -rf $RPM_BUILD_ROOT

make BUILDROOT=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/usr/bin/*
/usr/share/applnk/*
%dir /usr/share/doc/fldiff
/usr/share/doc/fldiff/*
/usr/share/man/*
/usr/share/icons/*

#
# End of "$Id$".
#
