//
// "$Id: FavoritesWindow.cxx 386 2006-03-04 14:15:27Z mike $"
//
// FavoritesWindows widget code.
//
// Copyright 2005 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Contents:
//
//

#include "FavoritesWindow.h"


//
// 'FavoritesWindow::FavoritesWindow()' - Create a favorites management window.
//

FavoritesWindow::FavoritesWindow()
  : Fl_Double_Window(355, 150, "Manage Favorites"),
    browser_(10, 10, 300, 95),
    up_(320, 10, 25, 25, "@8>"),
    delete_(320, 45, 25, 25, "X"),
    down_(320, 80, 25, 25, "@2>"),
    ok_(185, 115, 75, 25, "OK"),
    cancel_(270, 115, 75, 25, "Cancel")
{
  end();

  modal();

  browser_.type(FL_HOLD_BROWSER);
  browser_.when(FL_WHEN_RELEASE_ALWAYS);
  browser_.callback((Fl_Callback *)browser_cb, this);

  up_.callback((Fl_Callback *)up_cb, this);

  delete_.callback((Fl_Callback *)delete_cb, this);

  down_.callback((Fl_Callback *)down_cb, this);

  ok_.callback((Fl_Callback *)ok_cb, this);

  cancel_.callback((Fl_Callback *)cancel_cb, this);
}


//
// 'FavoritesWindow::browser_cb()' - Handle clicks in the favorites list.
//

void
FavoritesWindow::browser_cb(Fl_File_Browser *b,
					// I - Favorites browser
                            FavoritesWindow *fw)
					// I - Favorites window
{
  int		i;			// Current item


  i = fw->browser_.value();
  if (i)
  {
    if (i > 1)
      fw->up_.activate();
    else
      fw->up_.deactivate();

    fw->delete_.activate();

    if (i < fw->browser_.size())
      fw->down_.activate();
    else
      fw->down_.deactivate();
  }
  else
  {
    fw->up_.deactivate();
    fw->delete_.deactivate();
    fw->down_.deactivate();
  }
}


//
// 'FavoritesWindow::cancel_cb()' - Cancel (close) the window.
//

void
FavoritesWindow::cancel_cb(Fl_Button       *b,
					// I - Cancel button
                           FavoritesWindow *fw)
					// I - Favorites window
{
  fw->hide();
}


//
// 'FavoritesWindow::delete_cb()' - Delete the current directory.
//

void
FavoritesWindow::delete_cb(Fl_Button       *b,
					// I - Delete button
                           FavoritesWindow *fw)
					// I - Favorites window
{
  int		i;			// Current item


  i = fw->browser_.value();

  fw->browser_.remove(i);

  if (i > fw->browser_.size())
    i --;
  fw->browser_.select(i);

  if (i < fw->browser_.size())
    fw->down_.activate();
  else
    fw->down_.deactivate();

  if (i > 1)
    fw->up_.activate();
  else
    fw->up_.deactivate();

  if (!i)
    fw->delete_.deactivate();

  fw->ok_.activate();
}


//
// 'FavoritesWindow::down_cb()' - Move the current directory down one line.
//

void
FavoritesWindow::down_cb(Fl_Button       *b,
					// I - Down button
                         FavoritesWindow *fw)
					// I - Favorites window
{
  int		i;			// Current item


  i = fw->browser_.value();

  fw->browser_.insert(i + 2, fw->browser_.text(i), fw->browser_.data(i));
  fw->browser_.remove(i);
  fw->browser_.select(i + 1);

  if ((i + 1) == fw->browser_.size())
    fw->down_.deactivate();

  fw->up_.activate();

  fw->ok_.activate();
}


//
// 'FavoritesWindow::load()' - Load the favorites list.
//

void
FavoritesWindow::load()
{
  int		i;			// Looping var
  char		name[32],		// Preference name
		filename[1024];		// Directory in list
  Fl_Preferences prefs(Fl_Preferences::USER, "fltk.org", "filechooser");
					// File chooser preferences


  // Load the favorites list...
  browser_.clear();
  browser_.deselect();

  for (i = 0; i < 100; i ++)
  {
    // Get favorite directory 0 to 99...
    sprintf(name, "favorite%02d", i);

    prefs.get(name, filename, "", sizeof(filename));

    // Stop on the first empty favorite...
    if (!filename[0])
      break;

    // Add the favorite to the list...
    browser_.add(filename,
                 Fl_File_Icon::find(filename, Fl_File_Icon::DIRECTORY));
  }

  up_.deactivate();
  delete_.deactivate();
  down_.deactivate();
  ok_.deactivate();
}


//
// 'FavoritesWindow::ok_cb()' - Save the favorites list and close.
//

void
FavoritesWindow::ok_cb(Fl_Return_Button *b,
					// I - OK button
                       FavoritesWindow  *fw)
					// I - Favorites window
{
  fw->save();
  fw->hide();
}


//
// 'FavoritesWindow::save()' - Save the favorites list.
//

void
FavoritesWindow::save()
{
  int		i;			// Looping var
  char		name[32],		// Preference name
		filename[1024];		// Directory in list
  Fl_Preferences prefs(Fl_Preferences::USER, "fltk.org", "filechooser");
					// File chooser preferences


  // Copy the new list over...
  for (i = 0; i < browser_.size(); i ++)
  {
    // Set favorite directory 0 to 99...
    sprintf(name, "favorite%02d", i);

    prefs.set(name, browser_.text(i + 1));
  }

  // Clear old entries as necessary...
  for (; i < 100; i ++)
  {
    // Clear favorite directory 0 to 99...
    sprintf(name, "favorite%02d", i);

    prefs.get(name, filename, "", sizeof(filename));

    if (filename[0])
      prefs.set(name, "");
    else
      break;
  }
}


//
// 'FavoritesWindow::show()' - Load the favorites list and show.
//

void
FavoritesWindow::show()
{
  load();
  Fl_Double_Window::show();
}


//
// 'FavoritesWindow::up_cb()' - Move the current directory up one line.
//

void
FavoritesWindow::up_cb(Fl_Button       *b,
					// I - Up button
                       FavoritesWindow *fw)
					// I - Favorites window
{
  int		i;			// Current item


  i = fw->browser_.value();

  fw->browser_.insert(i - 1, fw->browser_.text(i), fw->browser_.data(i));
  fw->browser_.remove(i + 1);
  fw->browser_.select(i - 1);

  if (i == 2)
    fw->up_.deactivate();

  fw->down_.activate();

  fw->ok_.activate();
}


//
// End of "$Id: FavoritesWindow.cxx 386 2006-03-04 14:15:27Z mike $".
//
