#ifndef __PT_PROCESS_H
#define __PT_PROCESS_H
//========================================================================
// PtProcess.h
//========================================================================
// Portable Process with  pipes Handling
// Features easy File and Pipe/Process opening and reading uniformization 
// support of read "r" or write "w" popen modes
// Copyright F. Costantini 2006
// Version 1.0
//========================================================================
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//========================================================================

#ifdef WIN32
#include <windows.h>
#endif
#include <stdio.h>

class CPtProcess {
public:
	// construction / destruction
	CPtProcess();
	virtual ~CPtProcess();

	FILE * popen	(const char *cmd, const char *mode="r");
	FILE * fopen	(const char *file, const char *mode="r");

	int  close();

	bool is_open() const { return _fpt!=NULL ? true : false ;}
	FILE * desc() const { return _fpt;}
	
	char * get_line(char * line, size_t s) const {
		return _fpt ? fgets(line, s, _fpt) : NULL;
	}

#ifdef WIN32
public:
	static bool createPipe(HANDLE * h, BOOL bInheritHnd=TRUE);

protected:
	HANDLE pin[2], pout[2], perr[2];
	char ptmode;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
private:
	FILE * freeHandles();
	static void clean_close(HANDLE& h);
#endif

protected:
	FILE * _fpt;
};

#endif

