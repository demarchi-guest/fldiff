//
// "$Id: DiffChooser.h 388 2006-06-07 16:09:03Z fabien $"
//
// DiffChooser widget definitions.
//
// Copyright 2005 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef _DiffChooser_h_
#  define _DiffChooser_h_
#  include <FL/Fl.H>
#  include <FL/Fl_Group.H>
#  include <FL/Fl_File_Browser.H>
#  include <FL/Fl_File_Input.H>
#  include "FavoritesMenu.h"

#if defined(WIN32)
#include <io.h>
#define snprintf _snprintf
#define access _access
#else
#include <unistd.h>
#endif

class DiffChooser : public Fl_Group
{
  Fl_File_Input		input_;		// Input field
  FavoritesMenu		menu_;		// Favorites button
  Fl_File_Browser	browser_;	// File browser

  char			directory_[1024];// Directory name
  char			filename_[1024];// Cached filename

  static void	browser_cb(Fl_File_Browser *fb, DiffChooser *dc);
  static void	input_cb(Fl_File_Input *fi, DiffChooser *dc);
  static void	menu_cb(FavoritesMenu *fm, DiffChooser *dc);

  public:

		DiffChooser(int X, int Y, int W, int H, const char *L = (const char *)0);
  int		count();
  void		deselect() { browser_.deselect(); }
  const char	*directory() { return (directory_); }
  int		selected(int i) { return (browser_.selected(i)); }
  int		size() { return (browser_.size()); }
  void		value(const char *f);
  const char	*value() { return (input_.value()); }
  const char	*value(int i);
};


#endif // !_DiffChooser_h_

//
// End of "$Id: DiffChooser.h 388 2006-06-07 16:09:03Z fabien $".
//
