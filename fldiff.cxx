//
// "$Id: fldiff.cxx 407 2006-11-13 18:54:02Z mike $"
//
// A graphical diff program.
//
// Copyright 2005 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Contents:
//
//   main() - Show the differences between two files.
//

#include "DiffWindow.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//
// 'main()' - Show the differences between two files.
//

int					// O - Exit status
main(int  argc,				// I - Number of command-line args
     char *argv[])			// I - Command-line arguments
{
  DiffWindow	*dw;			// Diff window...


  // Use POSIX locale for all commands...
  putenv("LANG=C");

  // Use the GTK+ scheme...
  Fl::scheme("gtk+");

  // Create the window...
  if (argc > 1)
    dw = new DiffWindow(argv[1], argv[2]);
  else
    dw = new DiffWindow(NULL, NULL);

  dw->show(1, argv);

  // Run until all windows are closed...
  Fl::run();

  // Return with no errors...
  return (0);
}


//
// End of "$Id: fldiff.cxx 407 2006-11-13 18:54:02Z mike $".
//
